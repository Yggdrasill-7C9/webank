# 关于整合Vue项目、React项目、Angular项目为一个项目的解决方案

## 前言

关于整合三大框架的项目为一个项目的需求，我的第一反应就是微前端。

[微前端](https://www.thoughtworks.com/radar/techniques/micro-frontends)是由 ThoughtWorks 于2016年提出，将后端微服务的理念应用于浏览器端，即将 Web 应用由单一的单体应用转变为多个小型前端应用聚合为一的应用。

结合本项目的实际情况，较好的解决方案大概只分为两大类，一、利用 iFrame 嵌入不同框架的页面，隔离运行时，自定义消息传递机制；二、利用微前端的思想聚合其他项目。



## 利用 iFrame 技术实现业务需求

利用iFrame技术实际上是最好实现业务诉求的，但是缺点也很明显：

- 子项目需要改造，不能提供带导航的功能（因为iFrame本质上就是内嵌一个网页，由于内嵌网页导航的因素，用户体验会很差）

- iFrame 嵌入的显示区大小不容易控制
- iFrame 样式显示、兼容性问题，不单单是IE8的问题，chrome、Firefox之间也会存在差异
- iFrame 内嵌页面自适应iFrame容器大小的问题
- 性能开销因素。iframe加载的时候会阻塞主页面的onload，占用连接池，多层嵌套页面崩溃。
- bfcache问题。（bfcache就是浏览器会对页面回退进行缓存，如果用了iFrame就不是浏览器的缓存了，是iFrame与iFrame之间的落地，缓存不了。）url的记录完全无效，页面刷新不能够被记忆，刷新会返回首页，iFrame功能之间跳转也无效。说白了就是iFrame容器内的内容不能够准确被保留，刷新页面就回到解放前了，一定程度上说也是影响用户体验的。

## 利用微前端的思想聚合多个框架的项目

实现微前端的技术方式就有很多种了：包括自制框架兼容应用、利用Web Components技术构建应用、路由分发。

### 自制框架兼容应用（Single-SPA）

因为这一解决方案工作量极大，难度不亚于重新开发一套框架。时间成本很大，所以就不展开讨论了。

关于这一解决方案，具体可以参考 [Single-SPA](https://github.com/CanopyTax/single-spa) 、 [mooa](https://github.com/phodal/mooa)

### 利用Web Components技术

这个可以从在Web Components种构建现有框架和集成现有框架中的Web Components展开来讨论，

本项目只适合后一种方案：集成在现有框架中的 Web Components。

关于这种解决方案，可以参考下 [Stencil](https://github.com/ionic-team/stencil) , 就是将组建构建成Web Components形式的组建，随后在其他项目中直接引用。这个过程中会大量创造新的轮子，所以也不考虑。

## 外层框架用Angular搭建，内层组件用React、Angular、Vue，利用AST将三大框架的代码整合到一起

技术上是可行的，为没研究过，就不说太多了。

## 我的想法：发布静态资源+后台路由及服务

其实这个过程就是每一个功能独立独立打包、独立运行（Vue代码、React代码），通过总线注册机制把这些相应的功能串联起来，实现互相通信。

这里面（通讯机制）其实可以参考发布者-订阅者模式，这里的总线其实就相当于一个消息发布者，由他统一发布消息，然后Vue项目和React项目需要把这个消息注册给总线。

![我写的那个小demo](https://gitlab.com/Yggdrasill-7C9/webank/raw/master/1.png)

大概就是上图这个意思，React组件和Vue组件分别对应两个项目，其中React项目的计数器的值需要在Vue项目中显示，Vue项目计数器的值需要在React项目中显示，因为这里需要两个运行时，所以需要单独build好vue项目和React项目之后，将build好的文件引入到所需的html文件中（静态）

```html
<body>
    <div class="container">
        <header>nav</header>
        <hr>
        <div id="react-app"></div>
        <hr>
        <div id="vue-app">
            <vue-app></vue-app>
        </div>
    </div>
	<script type="text/babel" data-type="react-app">
        System.import("./micro/react-app-7c100655.js").then(_ => 		{
        let App = _.default;
        ReactDOM.render(
            <App/>,
            document.querySelector("#react-app"))
        })
  	</script>
    <script data-type="vue-app">
        new Vue({
            el: "#vue-app",
            components: {
                "vue-app": () => System.import("./micro/vue-app-6702fd79.js")
            }
        })
    </script>
</body
```

缺点：

- 这不是一套通用的解决方案，必须按照指定的规则编写代码
- 每次打包一个功能模块，都要手动把打包后的文件引入到指定的位置，虽然可以利用node.js写一个自动化构建工具来解决这个问题，但是缺点就是项目结构不能变，如果改变的话，那么自动化构建工具的代码也要改。
- 不支持热更新部署。每改一次代码都需要手动build一次，而且每次都要编译所有文件。

## 最后

以上吧，其实每种方案都有利弊，iframe可能更多要考虑cache问题、容器内容如何自适应容器大小的问题，琐碎的坑点比较多，而且兼容性不太好，很难保证在开发中会遇到其他意想不到的问题，而且要保证容器内子项目不能大概，否则会花费大量时间在适配上。优点就是短期内能出东西，但是要牺牲维护成本。

## 参考链接:

【1】用微前端的方式搭建类单页应用：https://tech.meituan.com/2018/09/06/fe-tiny-spa.html

【2】「微前端」- 将微服务理念扩展到前端开发（实战篇）：http://insights.thoughtworkers.org/micro-frontends-2/

【3】微前端那些事：https://github.com/phodal/microfrontends

【4】实施微前端的六种方式：https://juejin.im/post/5b45d0ea6fb9a04fa42f9f1a

【5】在react项目中集成angular.js：https://blog.csdn.net/smk108/article/details/80931303

